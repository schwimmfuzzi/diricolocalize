# README

## Build/Deploy
* git clone
* npm i
* evil muss gulp noch installiert werden, je nach euren Systemen - wenn ja: (sudo) npm i gulp -g
* bower i
* gulp (standard task zum bauen)
* Webserver (vhost, irgendwas) lokal zum starten der Anwendung, da sonst Chrome die calls auf JS-Dateien blockt, wenn die index.html aus dem Finder/Explorer gestartet wird. Aber ich vermute ihr habt eure Systeme alle besser im Griff, als ich meins gestern *räusper*

## Anmerkungen
* Gulp ist noch sehr chaotisch, wollte hier nicht zu viel Zeit verlieren.
* Für die Live Variante habe ich die bower_components nochmal in eine vendor.js gepackt, auf den lokal gebauten Versionen werden hier verschiedene Includes vorzufinden sein. Mit einem besser laufenden Gulp wäre das auch behoben.