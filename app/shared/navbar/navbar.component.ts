/// <reference path='../../_all.ts' />


module navbar {
	'use strict';

	

	angular.module('navbar',[])

		.component('navbar', {
			templateUrl: 'shared/navbar/navbar.component.html',
			controller: NavbarComponentCtrl,
			// require: {
			// 	GenerateLangFileCtrl: "^langkeysList"
			// }
		});

	function NavbarComponentCtrl($mdDialog, $mdMedia) {
		var $ctrl = this;
		console.log('navbar says hello');

		$ctrl.customFullscreen = $mdMedia('xs') || $mdMedia('sm');

		$ctrl.showGenerateFilesDialog = function(ev) {
			var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $ctrl.customFullscreen;
			$mdDialog.show({
				controller: GenerateLangFileCtrl,

				templateUrl: 'shared/dialogs/views/generateLangFile.tpl.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose: true,
				fullscreen: useFullScreen
			});
		};

		$ctrl.showAddLocalizationDialog = function (ev) {
			var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $ctrl.customFullscreen;
			$mdDialog.show({
				controller: AddLocalizationCtrl,

				templateUrl: 'shared/dialogs/views/addLocalizationDialog.tpl.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose: true,
				fullscreen: useFullScreen
			});
		};
	}

	function GenerateLangFileCtrl(localStorageService, $scope, $mdDialog) {
		var $ctrl = this;

		// todo: receive this dynamically
		$scope.availableLanguages = localStorageService.get('langAvailable');
		$ctrl.keys = localStorageService.get('langkeys');
		$scope.langFile = {};


		$scope.extractSingleLanguage = function() {
			angular.forEach($ctrl.keys, function(k){
				console.log(k);
				if (k.labels[$scope.selectedLang])
					$scope.langFile[k.key] = k.labels[$scope.selectedLang];
			});
		};

		$scope.cancel = function() {
			$mdDialog.cancel();
		};

	}


	function AddLocalizationCtrl(localStorageService, $scope, $mdDialog) {
		var $ctrl = this;
		console.log('hallo user');
		$ctrl.availableLanguages = localStorageService.get('langkeys');
		$ctrl.languages = localStorageService.get('langAvailable') || [];


		$scope.addLocalization = function() {
			var localeAlreadyThere = false;
			angular.forEach($ctrl.languages, function (l) {
				if(l.code === $scope.locale.code) {
					localeAlreadyThere = true;
				}
			});

			// only push things if the new locale was not there before
			if(!localeAlreadyThere) {
				$ctrl.languages.push($scope.locale);
				console.log($ctrl.languages);
				$ctrl.persistLanguages();
			}

			$mdDialog.cancel();
			$scope.locale = {};
		};

		$ctrl.persistLanguages = function () {
			localStorageService.set('langAvailable', $ctrl.languages);
		};

	}
}

