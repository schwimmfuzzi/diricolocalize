(function() {
    'use strict';


    angular
        .module('heros')

    .component('heroList', {
        templateUrl: 'components/heros/views/heroList.html',
        controller: HeroListComponentCtrl
    });

    function HeroListComponentCtrl(HeroService) {
        var selectedId = null;
        var $ctrl = this;

        this.$routerOnActivate = function(next) {
            // Load up the heroes for this view
            HeroService.getHeroes().then(function(heroes) {
                $ctrl.heroes = heroes;
                selectedId = next.params.id;
            });
        };

        this.isSelected = function(hero) {
            return (hero.id == selectedId);
        };
    }
}());
