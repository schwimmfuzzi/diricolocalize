(function(angular) {
    'use strict';
    angular.module('heros', [])

    .component('heros', {
        templateUrl: 'components/heros/views/heros.html',
        $routeConfig: [{
            path: '/',
            name: 'HeroList',
            component: 'heroList',
            useAsDefault: true
        }, {
            path: '/:id',
            name: 'HeroDetail',
            component: 'heroDetail'
        }]
    });
})(window.angular);
