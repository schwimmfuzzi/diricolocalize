(function() {
    'use strict';


    angular
        .module('crisis-center')

    .component('crisisList', {
        templateUrl: 'components/crisis/views/crisisList.html',
        bindings: { $router: '<' },
        controller: CrisisListComponentCtrl,
        $canActivate: function($nextInstruction, $prevInstruction) {
            console.log('$canActivate', arguments);
        }
    });


    function CrisisListComponentCtrl(CrisisService) {
        var selectedId = null;
        var ctrl = this;

        this.$routerOnActivate = function(next) {
            console.log('$routerOnActivate', this, arguments);
            // Load up the crises for this view
            CrisisService.getCrises().then(function(crises) {
                ctrl.crises = crises;
                selectedId = next.params.id;
            });
        };

        this.isSelected = function(crisis) {
            return (crisis.id == selectedId);
        };

        this.onSelect = function(crisis) {
            this.$router.navigate(['CrisisDetail', { id: crisis.id }]);
        };
    }
}());
