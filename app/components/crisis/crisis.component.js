(function(angular) {
    'use strict';
    angular.module('crisis-center', [])

    .component('crisisCenter', {
        templateUrl: 'components/crisis/views/crisisCenter.html',
        $routeConfig: [{
            path: '/',
            name: 'CrisisList',
            component: 'crisisList',
            useAsDefault: true
        }, {
            path: '/:id',
            name: 'CrisisDetail',
            component: 'crisisDetail'
        }]
    });
})(window.angular);
