/// <reference path='../../../_all.ts' />
module langkeys {
	'use strict';

	angular
		.module('langkeys')

		.component('langkeysList', {
			templateUrl: 'components/langkeys/views/langkeysList.component.html',
			controller: LangkeysListComponentCtrl
		});

	function LangkeysListComponentCtrl(localStorageService) {
		var $ctrl = this;

		// init stuff
		$ctrl.langDefault = 'en_US';
		$ctrl.langSelected = localStorageService.get('langSelected') || 'de_DE';
		$ctrl.langAvailable = localStorageService.get('langAvailable') || [];


		$ctrl.$routerOnActivate = function(next) {

			// just for displaying something while loading
			 $ctrl.keys = [{
	        	key: 'App',
	        	description: 'is loading the keys...',
	        	labels:{
	        		en_US: 'hang on please.'
	        	}
	        }];

	        // for purpose of empty list, let the user get a feeling what he will see
	        $ctrl.emptyKeyList = [{
        		key: 'FIRST_KEY',
        		description: 'you do not have any keys...',
        		labels: {
        			en_US: 'add your first one now!'
        		}
        	}];

            // Load up the heroes for this view
			console.log('langkeylist says hello');
			$ctrl.getKeys();
        };

       
        // get all keys from local storage
        $ctrl.getKeys = function () {
        	$ctrl.keys = localStorageService.get('langkeys') || $ctrl.emptyKeyList;
        };

		// add new row to keylist
        $ctrl.addKey = function () {
        	$ctrl.keys.push({});
        };

        // hook for saving action of a single key
        $ctrl.saveKey = function (item) {
        	// remove the dirty flag (this produces the save-checkmark at the end of the row)
        	item.dirty = false;
        	$ctrl.persistAllKeys();
        };

        // persist all existing keys to local storage
        $ctrl.persistAllKeys = function () {
        	localStorageService.set('langkeys', $ctrl.keys);
        	console.log('persisted all keys to localstorage');
        };

        // remove one item from the list
        $ctrl.removeKey = function (index) {
        	$ctrl.keys.splice(index, 1);
        	$ctrl.persistAllKeys();
        };

        // persisting the last choosen selected language to local storage. only to ease the use
        $ctrl.updateSelectedLanguage = function () {
        	localStorageService.set('langSelected', $ctrl.langSelected);
        }

        // toggle items that are completed
        $ctrl.toggleCompletedRedcords = function () {
        	var amt = $ctrl.langAvailable.length;
        	console.log('going to check if some rows can be hidden... availabe languages: ', amt);
        	angular.forEach($ctrl.keys, function (k) {
				console.log(Object.keys(k.labels).length);
        		// very naive test (lengths of languages), but faster that accessing every property
        		// theoretically, some unkown propery/key xyz inside k might count // -1 because of default language 
        		if($ctrl.hideCompleteRecords && (Object.keys(k.labels).length -1) === amt) {
        			k.hide = true;
        			console.log('i need to hide', k);
        		} else {
        			k.hide = false;
        		}
        	});
        };

	}
}
