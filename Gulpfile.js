// Load plugins
var gulp = require('gulp'),
sass = require('gulp-ruby-sass'),
less= require('gulp-less'),
autoprefixer = require('gulp-autoprefixer'),
cssnano = require('gulp-cssnano'),
jshint = require('gulp-jshint'),
uglify = require('gulp-uglify'),
imagemin = require('gulp-imagemin'),
inject = require('gulp-inject'),
rename = require('gulp-rename'),
concat = require('gulp-concat'),
notify = require('gulp-notify'),
ngAnnotate = require('gulp-ng-annotate'),
templateCache = require('gulp-angular-templatecache'),
cache = require('gulp-cache'),
tsc = require('gulp-tsc'),
livereload = require('gulp-livereload'),
del = require('del'),
path = require('path'),
series = require('stream-series'),
wiredep = require('wiredep').stream,
browserSync = require('browser-sync').create(),
vendor = require('gulp-concat-vendor'),
gulpsync = require('gulp-sync')(gulp);

// Styles
gulp.task('styles', function() {
  return gulp.src([
    // './app/**/*.less',
    // './assets/styles/*',
    'app/**/*.less'
    ])
  .pipe(less({
    paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
  .pipe(concat('style.css'))
  .pipe(gulp.dest('./build/styles')); 
});

gulp.task('copyBowerStyles', function () {
  return gulp.src([
    'bower_components/angular-material/angular-material.css',
    'bower_components/angular-material-icons/angular-material-icons.css'
    ])
  .pipe(gulp.dest('./build/styles')); 
  });

gulp.task('copyFontIcons', function () {
  return gulp.src([
    'bower_components/angular-material-icons/materialIcons.woff2'
    ])
  .pipe(gulp.dest('./build/fonts')); 
  });


// Scripts
gulp.task('scripts', ['typescriptCompile'],function() {
  return gulp.src([
    'compiledJS/**/*.js',
    // 'assets/**/*.js', 
    'app/**/*.js'])
  .pipe(jshint('.jshintrc'))
  .pipe(jshint.reporter('default'))
  // .pipe(concat('app.js'))
  .pipe(ngAnnotate())
  .pipe(rename({ suffix: '.min' }))
  .pipe(gulp.dest('build/scripts'))
  // .pipe(uglify())
  // .pipe(gulp.dest('build/scripts'))
  .pipe(notify({ message: 'Scripts task complete' }));
});

gulp.task('typescriptCompile', function(){
  return gulp.src(['app/**/*.ts'])
  .pipe(tsc())
  .pipe(gulp.dest('compiledJS'))
});

gulp.task('bower', function () {
  gulp.src('build/index.html')
  .pipe(wiredep())
  .pipe(gulp.dest('./build'));
});

// Images
gulp.task('images', function() {
  return gulp.src('src/images/**/*')
  .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
  .pipe(gulp.dest('build/images'))
  .pipe(notify({ message: 'Images task complete' }));
});

// Inject the files to index.html
gulp.task('inject', function () {
  var target = gulp.src('app/index.html');
  var styleStream = gulp.src(['./build/styles/**/*.css'], {read: false});
  var vendorStream = gulp.src([
    // './build/scripts/vendor.min.js',
    // './build/scripts/vendor/*.min.js',
    './build/vendor/*.min.js',
    './build/scripts/ngComponentRouter.min.js',
    './build/scripts/ngClipboard.min.js'
    ], {read: false});

  var appStream = gulp.src([
    './build/scripts/components/**/*.min.js', 
    '!build/scripts/**/*component.min.js'
    ], {read: false});

  // explicit stream for the component files
  var componentStream = gulp.src([
    './build/scripts/**/*component.min.js',
    '!./build/scripts/app.component.min.js'
    ], {read: false});

  // explicit stream for the component files
  var appMainStream = gulp.src([
    './build/scripts/app.component.min.js'
    ], {read: false});

  return target.pipe(inject(series(vendorStream,  componentStream, appMainStream, appStream, styleStream),{
    ignorePath: 'build',
    addRootSlash: false
  }))
  .pipe(gulp.dest('./build/'));
});

// Clean
gulp.task('clean', function() {
  return del(['build/*','compiledJS/*']);
});


// // Static server
// gulp.task('serve', function() {
//   browserSync.init({
//     server: {
//       baseDir: "/develop/ngBoilerplate"
//     }
//   });
// });

gulp.task('vendorScripts', function() {
  gulp.src('./bower_components/**/*.min.js')
  .pipe(vendor('vendor.min.js'))
  .pipe(gulp.dest('./build/vendor/'));  
});

gulp.task('templates', function () {
  return gulp.src('app/**/*.html')
    // .pipe(templateCache())
    .pipe(gulp.dest('build'));
});


// for more detailed example see 
// https://www.npmjs.com/package/gulp-sync
gulp.task('default', gulpsync.sync([
  'clean',
  'templates',
  'scripts',
  'vendorScripts',
  'copyBowerStyles',
  'copyFontIcons',
  'styles',
  'inject',
  'images',
  'bower'
]));


// Watch
gulp.task('watch', function() {

  // Watch .scss files
  gulp.watch('app/**/*.less', ['default']);

  // Watch .js files
  gulp.watch('app/**/*.ts', ['default']);

  // Watch image files
  gulp.watch('src/images/**/*', ['images']);

  // Create LiveReload server
  livereload.listen();

  // Watch any files in build/, reload on change
  gulp.watch(['build/**']).on('change', livereload.changed);
});
