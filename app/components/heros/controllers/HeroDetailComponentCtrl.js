(function() {
    'use strict';
    angular
        .module('heros')

    .component('heroDetail', {
        templateUrl: 'components/heros/views/heroDetail.html',
        bindings: { $router: '<' },
        controller: HeroDetailComponentCtrl
    });

    function HeroDetailComponentCtrl(HeroService) {
        var $ctrl = this;

        this.$routerOnActivate = function(next) {
            // Get the hero identified by the route parameter
            var id = next.params.id;
            HeroService.getHero(id).then(function(hero) {
                $ctrl.hero = hero;
            });
        };

        this.gotoHeroes = function() {
            var heroId = this.hero && this.hero.id;
            this.$router.navigate(['HeroList', { id: heroId }]);
        };
    }
}());
