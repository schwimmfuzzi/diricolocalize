/// <reference path='../../_all.ts' />
module langkeys {
	'use strict';

	angular.module('langkeys',[])

		.component('langkeys', {
			templateUrl: 'components/langkeys/langkeys.component.html',
			controller: LangkeysComponentCtrl,
			$routeConfig: [{
		        path: '/',
		        name: 'LangkeysList',
		        component: 'langkeysList',
		        useAsDefault: true
		      }]
		});

	function LangkeysComponentCtrl() {
		var $langkeyctrl = this;

		$langkeyctrl.$routerOnActivate = function(next) {
            // Load up the heroes for this view
			console.log('langkey says hello');
        };
	}
}