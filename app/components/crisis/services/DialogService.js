(function() {
    'use strict';


    angular
        .module('crisis-center')
        .service('DialogService', DialogService);

    function DialogService($q) {
        this.confirm = function(message) {
            return $q.when(window.confirm(message || 'Is it OK?'));
        };
    }
}());
