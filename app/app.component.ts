/// <reference path='_all.ts' />
module app{
  'use strict';
  angular.module('app', [
    'ngComponentRouter', 
    'ngMaterial',
    'LocalStorageModule',
    'ngClipboard',

    'heros', 
    'crisis-center',
    'navbar',
    'langkeys'
    ])

  .config(function($locationProvider, $mdThemingProvider) {
    $locationProvider.html5Mode(false);

    $mdThemingProvider.theme('default')
    .primaryPalette('indigo', {
      'hue-1': '100',
      'hue-2': '900',
      'hue-3': 'A100'
    })
    .accentPalette('pink');

  })

  .value('$routerRootComponent', 'app')

  .component('app', {
    templateUrl: 'app.html',
    $routeConfig: [{
        path: '/crisis-center/...', 
        name: 'CrisisCenter', 
        component: 'crisisCenter'
      },{
        path: '/heros/...', 
        name: 'Heros', 
        component: 'heros' 
      },{
        path: '/langkeys/...',
        name: 'Langkeys',
        component: 'langkeys',
        useAsDefault: true
      }]
  });
}